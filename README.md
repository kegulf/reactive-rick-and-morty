**Experis Academy Norway**

**Authors:**
* **Odd Martin Hansen**
  
# Reactive Rick and Morty
This project is a part of the Experis Academy Accelerated Learning program. 

It was built using [Create React App](https://github.com/facebook/create-react-app) and [The Rick and Morty API](https://rickandmortyapi.com).

The project lets you browse and search through episodes, characters and locations from the Rick and Morty universes.


Live version hosted on Firebase: [Reactive Rick and Morty](https://reactive-rick-and-morty.web.app/)


## Deviations from the assignments
The project does not follow the guidelines of the assignment complemetely. After discussions with the lecturer I were allowed to have some artistic liberties.

The naming of components does not match does what the task asks for, due to the requested component names being too similar, e.g. `CardPage` and `CardsPage`.

The project also implements a few more features than the assignment asks for, this due to the project-authors love of both Reactjs and Rick and Morty.

Extra functionality 
- Search and browse `Episodes`
- Links to `Locations` / `Episodes` / `Characters` in the `SingleCharacter`, `SingleLocation` and `SingleEpisode` pages.
- Home page with description
- Use of an [Infinite scroller component](https://www.npmjs.com/package/react-infinite-scroller) 
## Assignment description
Follow the slides to create a Rick and Morty Character Card web app with React.

The app should be populated with data from the Rick and Morty API.

* You should have a page that displays all the character cards and a page that displays a single character card based on an ID parameter in the path.
   
* The character card in CardsPage should have a link but the CharacterCard in CardPage should not display the link (hint: look for the `showLink` property in the slides)
  
* On your CardsPage you should have a search bar that filters the list of characters.
  
* Repeat the same process for the `locations` part of the API. See the slides for details.

If you have run into the 429 Limit Exceeded error then you need to change your internet connection to access the API again (such as using your phone as a mobile hotspot).

Resources:

Basic React-Router use: https://reacttraining.com/react-router/web/example/basic

Get URL parameters (`/character/:id`) inside your components: https://reacttraining.com/react-router/web/example/url-params