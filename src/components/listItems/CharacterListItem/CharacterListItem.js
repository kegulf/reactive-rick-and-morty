import PropTypes from "prop-types";
import React from "react";
import { Link } from "react-router-dom";

const CharacterListItem = ({ character }) => (
	<Link to={"/characters/" + character.id} className="list-item character-list-item">
		<img
			src={character.image}
			alt={"Photo of " + character.name}
			style={{
				width: "60px",
				height: "60px",
				float: "left",
				marginRight: "15px"
			}}
		/>
		<div>
			<p>
				<strong>{character.name}</strong>
			</p>
			<p>
				<span>Status: {character.status}</span>
				<span>Species: {character.species}</span>
			</p>
		</div>
	</Link>
);

CharacterListItem.propTypes = {
	character: PropTypes.object.isRequired
};

export default CharacterListItem;
