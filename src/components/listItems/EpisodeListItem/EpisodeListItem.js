import PropTypes from "prop-types";
import React from "react";

import { Link } from "react-router-dom";

const EpisodeListItem = ({ episode }) => {
	const seasonNumber = Number(episode.episode.slice(1, 3));
	const episodeNumber = Number(episode.episode.slice(4, 6));

	return (
		<Link to={"/episodes/" + episode.id} className="list-item episode-list-item">
			<p>
				<strong>{episode.name}</strong>
			</p>
			<p>
				<span>Season: {seasonNumber}</span>
				<span>Episode: {episodeNumber}</span>
			</p>
		</Link>
	);
};

EpisodeListItem.propTypes = { episode: PropTypes.object.isRequired };

export default EpisodeListItem;
