import PropTypes from "prop-types";
import React from "react";

import { Link } from "react-router-dom";

const LocationListItem = ({ location }) => {
	return (
		<Link to={"/locations/" + location.id} className="list-item location-list-item">
			<p>
				<strong>{location.name}</strong>
			</p>
			<p>
				<span>Type: {location.type || "-"}</span>
				<span>Dimension: {location.dimension || "-"}</span>
			</p>
		</Link>
	);
};

LocationListItem.propTypes = { location: PropTypes.object.isRequired };

export default LocationListItem;
