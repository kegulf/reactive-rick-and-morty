import React from "react";

import PropTypes from "prop-types";

import { Link } from "react-router-dom";

const CharacterCard = props => {
	const { id, name, status, species, type, gender, origin, location, image } = props.character;

	/**
	 * Creates a link if the location object has a url.
	 * The last part of the Url contains the id of the loaction.
	 * The ID is splitted out and appended to the locations url
	 * @returns the location name or a url string on format: "/locations/42"
	 */
	const getLocationLinkIfNotUnknown = location => {
		if (location.url) {
			const url = "/locations/" + location.url.split("/").slice(-1)[0];
			return <Link to={url}>{location.name}</Link>;
		}

		return location.name;
	};

	const cardTextClasses = "m-0 p-2 border-bottom border-secondary ";

	return (
		<div
			className="d-inline-block m-2 card bg-dark text-white"
			style={{ borderRadius: "6px", width: "350px" }}
		>
			<Link to={"/characters/" + id}>
				<img className="card-img-top rounded-top" src={image} alt={"image of " + name} />
			</Link>
			<p className={cardTextClasses}>Name: {name || "-"}</p>
			<p className={cardTextClasses}>Status: {status || "-"}</p>
			<p className={cardTextClasses}>Speicies: {species || "-"}</p>
			<p className={cardTextClasses}>Type: {type || "-"}</p>
			<p className={cardTextClasses}>Gender: {gender || "-"}</p>
			<p className={cardTextClasses}>Origin: {getLocationLinkIfNotUnknown(origin)}</p>
			<p className={cardTextClasses}>Last seen: {getLocationLinkIfNotUnknown(location)}</p>
			<Link className="btn btn-primary w-100 " to={"/characters/" + id}>
				View
			</Link>
		</div>
	);
};

CharacterCard.propTypes = {
	character: PropTypes.object.isRequired
};

export default CharacterCard;
