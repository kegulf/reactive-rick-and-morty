import PropTypes from "prop-types";
import React from "react";

import InfiniteScroll from "react-infinite-scroller";

import FormControl from "react-bootstrap/FormControl";
import LoadingSpinner from "../../LoadingSpinner/LoadingSpinner";

const CardsPanelContainer = props => {
	const { fetchData, hasMore, children, searchQuery, setSearchQuery } = props;

	return (
		<>
			<FormControl
				style={{ width: "352px", margin: "15px auto" }}
				type="text"
				placeholder="Search"
				className=""
				value={searchQuery}
				onChange={e => setSearchQuery(e.target.value)}
			/>
			<InfiniteScroll
				style={{ textAlign: "center" }}
				pageStart={0}
				loadMore={fetchData}
				hasMore={hasMore}
				loader={<LoadingSpinner key={0} />}
			>
				{children}
			</InfiniteScroll>
		</>
	);
};

CardsPanelContainer.propTypes = {
	fetchData: PropTypes.func.isRequired,
	hasMore: PropTypes.bool.isRequired,
	searchQuery: PropTypes.string.isRequired,
	setSearchQuery: PropTypes.func.isRequired
};

export default CardsPanelContainer;
