import React from "react";

import Navbar from "react-bootstrap/Navbar";
import Nav from "react-bootstrap/Nav";

import { NavLink } from "react-router-dom";

import logo from "./../../../resources/images/rick-and-morty-png-transparent.png";
import * as routes from "./../../../constants/routes";

const Toolbar = () => {
	return (
		<Navbar className="site-navbar" bg="dark" variant="dark">
			<NavLink className="navbar-brand" exact to={routes.HOME}>
				<img style={{ height: "50px" }} src={logo} alt="Rick and morty logo" />
			</NavLink>
			<Nav className="mr-auto">
				<NavLink className="nav-link" to={routes.CHARACTERS}>
					Characters
				</NavLink>
				<NavLink className="nav-link" to={routes.LOCATIONS}>
					Locations
				</NavLink>
				<NavLink className="nav-link" to={routes.EPISODES}>
					Episodes
				</NavLink>
			</Nav>
		</Navbar>
	);
};

export default Toolbar;
