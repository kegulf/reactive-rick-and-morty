import React from "react";

const Footer = () => {
	return (
		<footer className="site-footer w-100 text-center bg-dark text-white">
			Copyright &copy; 2019, Odd Martin Hansen
		</footer>
	);
};

export default Footer;
