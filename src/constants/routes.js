export const HOME = "/";

export const CHARACTERS = "/characters";
export const SINGLE_CHARACTER = "/characters/:characterId";

export const EPISODES = "/episodes";
export const SINGLE_EPISODE = "/episodes/:episodeId";

export const LOCATIONS = "/locations";
export const SINGLE_LOCATION = "/locations/:locationId";
