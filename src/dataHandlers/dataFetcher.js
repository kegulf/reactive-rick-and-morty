const apiUrl = "https://rickandmortyapi.com/api/";

const characterUrl = apiUrl + "character";
const locationUrl = apiUrl + "location";
const episodeUrl = apiUrl + "episode";

// -------------------------------------
// 			 Generic Fetchers
// -------------------------------------

const doSingleItemFetch = (url, id) => {
	return fetch(`${url}/${id}`).then(response => response.json());
};

const doAllItemsFetch = (url, pageNumber) => {
	return fetch(`${url}/?page=${pageNumber}`).then(response => response.json());
};

const doMultipleItemsFetch = (url, idList) => {
	return fetch(`${url}/${idList}`).then(response => response.json());
};

const doSimpleSearchByName = (url, name) => {
	return fetch(`${url}/?name=${name}`).then(response => response.json());
};

/**
 * Generates the url for the search query.
 *
 * @param {String} url the base url of the search (/episode/, /location/, /character/)
 * @param {Object} searchParams object containing the search query arguments
 */
const doAdvancedSearch = (url, searchParams) => {
	let queryString = "";

	for (let key in searchParams) {
		queryString += queryString.length === 0 ? "?" : "&";
		queryString += `${key}=${searchParams[key]}`;
	}

	return fetch(`${url}/${queryString}`).then(response => response.json());
};

// -------------------------------------
// 			    Characters
// -------------------------------------

/** @param {Number} id the id of the character to fetch */
export const getSingleCharacter = id => {
	return doSingleItemFetch(characterUrl, id);
};

/** @param {Number} pageNumber current page to fetch (pagination) */
export const getAllCharacters = pageNumber => {
	return doAllItemsFetch(characterUrl, pageNumber);
};

/**
 * @param {*} idList either comma seperated id string or an Array of ids
 *
 */
export const getMultipleCharacters = idList => {
	return doMultipleItemsFetch(characterUrl, idList);
};

/** @param {string} name the name to search for */
export const getCharacterNameSearchResult = name => {
	return doSimpleSearchByName(characterUrl, name);
};

/**
 * Takes in an object containing search params from the searchBar component
 * @param {Object} searchParams
 */
export const getCharacterAdvancedSearchResult = searchParams => {
	return doAdvancedSearch(characterUrl, searchParams);
};

// -------------------------------------
// 				Locations
// -------------------------------------

/** @param {Number} id id of the location to fetch */
export const getSingleLocation = id => {
	return doSingleItemFetch(locationUrl, id);
};

/** @param {Number} pageNumber current page number (pagination) */
export const getAllLocations = pageNumber => {
	return doAllItemsFetch(locationUrl, pageNumber);
};

/** @param {*} idList either comma seperated id string or an Array of ids */
export const getMultipleLocations = idList => {
	return doMultipleItemsFetch(locationUrl, idList);
};

/** @param {string} name the name to search for */
export const getLocationNameSearchResult = name => {
	return doSimpleSearchByName(locationUrl, name);
};

/**
 * Takes in an object containing search params from the searchBar components
 * @param {*} searchParams parameters of the search query */
export const getLocationAdvancedSearchResult = searchParams => {
	return doAdvancedSearch(locationUrl, searchParams);
};

// -------------------------------------
// 				 Episodes
// -------------------------------------

/** @param {Number} id the id of the episode to fetch */
export const getSingleEpisode = id => {
	return doSingleItemFetch(episodeUrl, id);
};

/** @param {Number} pageNumber the page to fecth (pagination) */
export const getAllEpisodes = pageNumber => {
	return doAllItemsFetch(episodeUrl, pageNumber);
};

/** @param {*} idList either comma seperated id string or an Array of ids */
export const getMultipleEpisodes = idList => {
	return doMultipleItemsFetch(episodeUrl, idList);
};

/** @param {string} name the name of the episode to search for */
export const getEpisodeNameSearchResult = name => {
	return doSimpleSearchByName(episodeUrl, name);
};

/**
 * Takes in an object containing search params from the searchBar components
 * @param {*} searchParams parameters of the search query */
export const getEpisodeAdvancedSearchResult = searchParams => {
	return doAdvancedSearch(episodeUrl, searchParams);
};
