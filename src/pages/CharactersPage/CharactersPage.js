import React, { useState } from "react";

import { getAllCharacters } from "./../../dataHandlers/dataFetcher";
import { CharacterCard, CardsPanelContainer } from "./../../components";

const CharactersPage = () => {
	const [characters, setCharacters] = useState([]);
	const [hasMore, setHasMore] = useState(true);
	const [searchQuery, setSearchQuery] = useState("");

	const fetchCharacters = page => {
		getAllCharacters(page)
			.then(data => {
				if (data.info && data.results) {
					setHasMore(page < data.info.pages);
					setCharacters(prevState => [...prevState, ...data.results]);
				}
			})
			.catch(error => {
				console.log(error);
			});
	};

	return (
		<main className="site-content" style={{ maxWidth: "1200px" }}>
			<header>
				<h1>Characters page</h1>
			</header>

			<CardsPanelContainer
				hasMore={hasMore}
				fetchData={fetchCharacters}
				searchQuery={searchQuery}
				setSearchQuery={setSearchQuery}
			>
				{characters
					.filter(character => character.name.toLowerCase().includes(searchQuery))
					.map(character => (
						<CharacterCard key={character.id} character={character} />
					))}
			</CardsPanelContainer>
		</main>
	);
};

export default CharactersPage;
