import React, { useState, useEffect } from "react";

import { getSingleEpisode, getMultipleCharacters } from "./../../dataHandlers/dataFetcher";
import { LoadingSpinner, CharacterListItem } from "../../components";

const SingleEpisodePage = props => {
	const [episodeData, setEpisodeData] = useState(null);
	const [episodeCharacters, setEpisodeCharacters] = useState([]);
	const [loading, setLoading] = useState(false);

	const episodeId = props.match.params.episodeId;

	// useEffect is a React Hook that allows
	// us to implement componentDidMount / componentWillMount / componentWillUpdate
	// With the current setup of this method, it will run when ever episodeId
	// changes (which only happens on initial load)
	useEffect(() => {
		setLoading(true);

		getSingleEpisode(episodeId)
			.then(data => {
				setEpisodeData(data);

				const characterIds = data.characters.map(
					characterUrl => characterUrl.split("/").slice(-1)[0]
				);

				getMultipleCharacters(characterIds)
					.then(data => {
						setEpisodeCharacters(data);
					})
					.finally(() => {
						setLoading(false);
					});
			})
			.catch(error => {
				console.log(error);
				setLoading(false);
			});
	}, [episodeId]);
	//   ^^ Dependency array. Will run the useEffect
	//   function when the values in the array changes*/

	// If no episode data was gotten when loading ends
	if (!episodeData && !loading) {
		return (
			<main className="site-content">
				<h1>Could not find that Episode</h1>
			</main>
		);
	} else if (loading) {
		return (
			<main className="site-content">
				<LoadingSpinner />
			</main>
		);
	} else if (episodeData.error) {
		return (
			<main className="site-content">
				<h1>{episodeData.error}</h1>
			</main>
		);
	} else if (episodeData.id) {
		const { name, air_date, episode } = episodeData;
		return (
			<main className="site-content">
				<h1>
					{episode} - {name}
				</h1>
				<p>Aired: {air_date}</p>
				<p>Unique character apperances: {episodeCharacters.length}</p>
				<h4>Characters</h4>
				{episodeCharacters.map(character => (
					<CharacterListItem key={character.id} character={character} />
				))}
			</main>
		);
	}
};

export default SingleEpisodePage;
