import HomePage from "./HomePage/HomePage";

import CharactersPage from "./CharactersPage/CharactersPage";
import SingleCharacterPage from "./SingleCharacterPage/SingleCharacterPage";

import LocationsPage from "./LocationsPage/LocationsPage";
import SingleLocationPage from "./SingleLocationPage/SingleLocationPage";

import EpisodesPage from "./EpisodesPage/EpisodesPage";
import SingleEpisodePage from "./SingleEpisodePage/SingleEpisodePage";

export {
	HomePage,
	CharactersPage,
	SingleCharacterPage,
	LocationsPage,
	SingleLocationPage,
	EpisodesPage,
	SingleEpisodePage
};
