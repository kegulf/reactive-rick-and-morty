import React, { useState, useEffect } from "react";

import { getSingleLocation, getMultipleCharacters } from "./../../dataHandlers/dataFetcher";
import { LoadingSpinner, CharacterListItem } from "../../components";

const SingleLocationPage = props => {
	const [locationData, setLocationData] = useState(null);
	const [residents, setResidents] = useState([]);
	const [loading, setLoading] = useState(false);

	const locationId = props.match.params.locationId;

	useEffect(() => {
		setLoading(true);

		getSingleLocation(locationId)
			.then(data => {
				setLocationData(data);

				const residentIds = data.residents.map(
					residentUrl => residentUrl.split("/").slice(-1)[0]
				);

				getMultipleCharacters(residentIds)
					.then(data => {
						// If several residents, the dataset is an array
						if (Array.isArray(data)) {
							setResidents(data);
						}

						// If only one resident, the dataset is an object representing
						// that resident
						else {
							setResidents([data]);
						}
					})
					.finally(() => {
						setLoading(false);
					});
			})
			.catch(error => {
				setLoading(false);
				console.log(error);
			});
	}, [locationId]);
	//  ^^ Dependency array, useEffect will trigger when the contents
	//     of the array changes. (only once in this case)

	if (!locationData && !loading) {
		return (
			<main className="site-content">
				<h1>Could not find that Location</h1>
			</main>
		);
	} else if (loading) {
		return (
			<main className="site-content">
				<LoadingSpinner />
			</main>
		);
	} else if (locationData.error) {
		return (
			<main className="site-content">
				<h1>{locationData.error}</h1>
			</main>
		);
	} else if (locationData.id && !loading) {
		const { name, type, dimension } = locationData;
		return (
			<main className="site-content">
				<h1>{name}</h1>
				<p>Type: {type}</p>
				<p>Dimension: {dimension}</p>
				<p>Residents: {residents.length}</p>
				{residents.map(resident => (
					<CharacterListItem key={resident.id} character={resident} />
				))}
			</main>
		);
	}
};

export default SingleLocationPage;
