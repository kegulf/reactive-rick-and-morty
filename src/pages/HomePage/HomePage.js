import React from "react";

const HomePage = props => {
	return (
		<main className="site-content">
			<header style={{ textAlign: "center" }}>
				<h1>Reactive Rick and Morty</h1>
				<p style={{ margin: "0" }}>
					<i>A Reactjs project by Odd Martin Hansen.</i>
				</p>
				<p>
					<i>
						Using{" "}
						<a
							href="https://github.com/facebook/create-react-app"
							target="_blank"
							rel="noopener noreferrer"
						>
							Create React App
						</a>{" "}
						and{" "}
						<a href="https://rickandmortyapi.com/" target="_blank" rel="noopener noreferrer">
							the Rick and Morty api
						</a>
					</i>
				</p>
			</header>

			<p>
				This application lets you browse characters, episodes and locations from the Rick and
				Morty universes.
			</p>
			<p>
				As of now the searching only let's you search for the names of the characters,
				locations and episodes, but there is plans to implement an advanced search as well that
				allows for search by all info about the characters.
			</p>
		</main>
	);
};

export default HomePage;
