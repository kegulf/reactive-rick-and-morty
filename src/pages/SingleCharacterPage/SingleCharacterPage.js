import React, { useState, useEffect } from "react";

import { Link } from "react-router-dom";

import { getSingleCharacter, getMultipleEpisodes } from "../../dataHandlers/dataFetcher";
import { LoadingSpinner, EpisodeListItem } from "../../components/";

const SingleCharacterPage = props => {
	const [characterData, setCharacterData] = useState(null);
	const [episodes, setEpisodes] = useState([]);
	const [loading, setLoading] = useState(false);

	const characterId = props.match.params.characterId;

	useEffect(() => {
		setLoading(true);

		getSingleCharacter(characterId)
			.then(data => {
				setCharacterData(data);

				const idList = data.episode.map(episode => episode.split("/").slice(-1)[0]);

				getMultipleEpisodes(idList.join(",")).then(episodeData => {
					if (Array.isArray(episodeData)) {
						setEpisodes(episodeData);
					} else {
						setEpisodes([episodeData]);
					}
					setLoading(false);
				});
			})
			.catch(error => {
				setLoading(false);
				console.log(error);
			});
	}, [characterId]);

	if (!characterData && !loading) {
		return (
			<main className="site-content">
				<h1>Could not find that character</h1>
			</main>
		);
	} else if (loading) {
		return (
			<main className="site-content">
				<LoadingSpinner />
			</main>
		);
	} else if (characterData.error) {
		return (
			<main className="site-content">
				<h1>{characterData.error}</h1>
			</main>
		);
	} else if (characterData.id) {
		const { name, status, species, type, gender, origin, location, image } = characterData;

		const getLocationLinkIfNotUnknown = obj => {
			if (obj.url) {
				const url = "/locations/" + obj.url.split("/").slice(-1)[0];
				return <Link to={url}>{obj.name}</Link>;
			}

			return obj.name;
		};

		return (
			<main className="site-content">
				<h1>{name}</h1>
				<section>
					<img
						style={{ width: "225px", float: "right" }}
						src={image}
						alt={"Image of " + name}
					/>
					<p>Status: {status}</p>
					<p>Species: {species || "-"}</p>
					<p>Type: {type || "-"}</p>
					<p>Gender: {gender || "-"}</p>
					<p>Origin: {getLocationLinkIfNotUnknown(origin)}</p>
					<p>Last seen: {getLocationLinkIfNotUnknown(location)}</p>
				</section>
				<section style={{ clear: "both", paddingTop: "20px" }}>
					<p>
						<strong>Appears in:</strong>
					</p>
					{episodes.map(ep => (
						<EpisodeListItem key={ep.id} episode={ep} />
					))}
				</section>
			</main>
		);
	}
};

SingleCharacterPage.propTypes = {};

export default SingleCharacterPage;
