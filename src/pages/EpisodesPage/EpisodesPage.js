import React, { useState } from "react";

import { getAllEpisodes } from "./../../dataHandlers/dataFetcher";
import { CardsPanelContainer, EpisodeListItem } from "./../../components";

const EpisodesPage = () => {
	const [episodes, setEpisodes] = useState([]);
	const [hasMore, setHasMore] = useState(true);
	const [searchQuery, setSearchQuery] = useState("");

	const fetchEpisodes = page => {
		getAllEpisodes(page)
			.then(data => {
				setHasMore(page < data.info.pages);
				setEpisodes(prevState => [...prevState, ...data.results]);
			})
			.catch(error => {
				console.log(error);
			});
	};

	return (
		<main className="site-content">
			<header>
				<h1>Episodes page</h1>
			</header>

			<CardsPanelContainer
				hasMore={hasMore}
				fetchData={fetchEpisodes}
				searchQuery={searchQuery}
				setSearchQuery={setSearchQuery}
			>
				{episodes
					.filter(
						episode =>
							episode.name.toLowerCase().includes(searchQuery) ||
							episode.episode.toLowerCase().includes(searchQuery)
					)
					.map(episode => (
						<EpisodeListItem key={episode.id} episode={episode} />
					))}
			</CardsPanelContainer>
		</main>
	);
};

export default EpisodesPage;
