import React, { useState } from "react";

import { getAllLocations } from "./../../dataHandlers/dataFetcher";
import { CardsPanelContainer, LocationListItem } from "./../../components";

const LocationsPage = () => {
	const [locations, setLocations] = useState([]);
	const [hasMore, setHasMore] = useState(true);
	const [searchQuery, setSearchQuery] = useState("");

	const fetchLocations = page => {
		getAllLocations(page)
			.then(data => {
				setHasMore(page < data.info.pages);
				setLocations(prevState => [...prevState, ...data.results]);
			})
			.catch(error => {
				console.log(error);
			});
	};

	return (
		<main className="site-content">
			<header>
				<h1>Locations page</h1>
			</header>

			<CardsPanelContainer
				hasMore={hasMore}
				fetchData={fetchLocations}
				searchQuery={searchQuery}
				setSearchQuery={setSearchQuery}
			>
				{locations
					.filter(location => location.name.toLowerCase().includes(searchQuery))
					.map(location => (
						<LocationListItem key={location.id} location={location} />
					))}
			</CardsPanelContainer>
		</main>
	);
};

export default LocationsPage;
