import React from "react";

import { BrowserRouter, Route } from "react-router-dom";

import { Toolbar, Footer } from "./components";
import {
	HomePage,
	CharactersPage,
	SingleCharacterPage,
	EpisodesPage,
	SingleEpisodePage,
	LocationsPage,
	SingleLocationPage
} from "./pages";

import * as routes from "./constants/routes";

const App = () => {
	return (
		<BrowserRouter>
			<Toolbar />

			<Route exact path={routes.HOME} component={HomePage} />

			<Route exact path={routes.CHARACTERS} component={CharactersPage} />
			<Route path={routes.SINGLE_CHARACTER} component={SingleCharacterPage} />

			<Route exact path={routes.LOCATIONS} component={LocationsPage} />
			<Route path={routes.SINGLE_LOCATION} component={SingleLocationPage} />

			<Route exact path={routes.EPISODES} component={EpisodesPage} />
			<Route path={routes.SINGLE_EPISODE} component={SingleEpisodePage} />

			<Footer />
		</BrowserRouter>
	);
};

export default App;
